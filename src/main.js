import Vue from 'vue'
import App from './App.vue'

import Bootstrap from 'bootstrap'
import Style from './style/app.scss'

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');

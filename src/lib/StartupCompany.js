
const Employees = {
    Designer: { title: "Designer", color: "success" },
    Developer: { title: "Developer", color: "danger" },
    LeadDeveloper: { title: "Lead Developer", color: "primary" },
    SysAdmin: { title: "SysAdmin", color: "secondary" },
};

const Levels = {
    Beginner: 0,
    Intermediate: 1,
    Expert: 2,
};

const Pieces = {
    // Developer
    UIComponent: { name: "UI Component", employee: Employees.Developer, level: Levels.Beginner, time: 2 },
    BackendComponent: { name: "Backend Component", employee: Employees.Developer, level: Levels.Beginner, time: 4 },
    NetworkComponent: { name: "Network Component", employee: Employees.Developer, level: Levels.Beginner, time: 6 },
    DatabaseComponent: { name: "Database Component", employee: Employees.Developer, level: Levels.Beginner, time: 4 },
    VideoComponent: { name: "Video Component", employee: Employees.Developer, level: Levels.Beginner, time: 14 },
    SemanticComponent: { name: "Semantic Component", employee: Employees.Developer, level: Levels.Intermediate, time: 3 },
    EncryptionComponent: { name: "Encryption Component", employee: Employees.Developer, level: Levels.Intermediate, time: 8 },
    FilesystemComponent: { name: "Filesystem Component", employee: Employees.Developer, level: Levels.Intermediate, time: 4 },
    SmtpComponent: { name: "Smtp Component", employee: Employees.Developer, level: Levels.Intermediate, time: 8 },
    i18nComponent: { name: "i18n Component", employee: Employees.Developer, level: Levels.Expert, time: 4 },
    SearchAlgorithmComponent: { name: "Search Algorithm Component", employee: Employees.Developer, level: Levels.Expert, time: 12 },
    CompressionComponent: { name: "Compression Component", employee: Employees.Developer, level: Levels.Expert, time: 8 },
    // Designer
    BlueprintComponent: { name: "Blueprint Component", employee: Employees.Designer, level: Levels.Beginner, time: 2 },
    WireframeComponent: { name: "Wireframe Component", employee: Employees.Designer, level: Levels.Beginner, time: 3 },
    GraphicsComponent: { name: "Graphics Component", employee: Employees.Designer, level: Levels.Beginner, time: 4 },
    UIElement: { name: "UI Element", employee: Employees.Designer, level: Levels.Beginner, time: 6 },
    UISet: { name: "UI Set", employee: Employees.Designer, level: Levels.Intermediate, time: 18 },
    ResponsiveUI: { name: "Responsive UI", employee: Employees.Designer, level: Levels.Intermediate, time: 18 },
    DocumentationComponent: { name: "Documentation Component", employee: Employees.Designer, level: Levels.Expert, time: 12 },
    DesignGuidelines: { name: "Design Guidelines", employee: Employees.Designer, level: Levels.Expert, time: 90 },
    // Lead Developer
    InterfaceModule: { name: "Interface Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 15 },
    FrontendModule: { name: "Frontend Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 21 },
    BackendModule: { name: "Backend Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 10 },
    InputModule: { name: "Input Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 6 },
    StorageModule: { name: "Storage Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 8 },
    ContentManagementModule: { name: "Content Management Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 37 },
    VideoPlaybackModule: { name: "Video Playback Module", employee: Employees.LeadDeveloper, level: Levels.Beginner, time: 45 },
    SeoModule: { name: "Seo Module", employee: Employees.LeadDeveloper, level: Levels.Intermediate, time: 24 },
    AuthenticationModule: { name: "Authentication Module", employee: Employees.LeadDeveloper, level: Levels.Intermediate, time: 22 },
    EmailModule: { name: "Email Module", employee: Employees.LeadDeveloper, level: Levels.Intermediate, time: 12 },
    DatabaseLayer: { name: "Database Layer", employee: Employees.LeadDeveloper, level: Levels.Intermediate, time: 14},
    NotificationModule: { name: "Notification Module", employee: Employees.LeadDeveloper, level: Levels.Intermediate, time: 18 },
    PaymentGatewayModule: { name: "Payment Gateway Module", employee: Employees.LeadDeveloper, level: Levels.Expert, time: 76 },
    LocalizationModule: { name: "Localization Module", employee: Employees.LeadDeveloper, level: Levels.Expert, time: 25 },
    SearchModule: { name: "Search Module", employee: Employees.LeadDeveloper, level: Levels.Expert, time: 20 },
    BandwidthCompressionModule: { name: "Bandwidth Compression Module", employee: Employees.LeadDeveloper, level: Levels.Expert, time: 18 },
    ApiClientModule: { name: "ApiClient Module", employee: Employees.LeadDeveloper, level: Levels.Expert, time: 40 },
    CodeOptimizationModule: { name: "Code Optimization Module", employee: Employees.LeadDeveloper, level: Levels.Expert, time: 90 },
    // SysAdmin
    VirtualHardware: { name: "Virtual Hardware", employee: Employees.SysAdmin, level: Levels.Beginner, time: 4 },
    OperatingSystem: { name: "Operating System", employee: Employees.SysAdmin, level: Levels.Beginner, time: 4 },
    Firewall: { name: "Firewall", employee: Employees.SysAdmin, level: Levels.Beginner, time: 4 },
    ProcessManagement: { name: "Process Management", employee: Employees.SysAdmin, level: Levels.Intermediate, time: 6 },
    ContinuousIntegration: { name: "Continuous Integration", employee: Employees.SysAdmin, level: Levels.Intermediate, time: 8 },
    CronJob: { name: "Cron Job", employee: Employees.SysAdmin, level: Levels.Intermediate, time: 3 },
    VirtualContainer: { name: "Virtual Container", employee: Employees.SysAdmin, level: Levels.Expert, time: 25 },
    Cluster: { name: "Cluster", employee: Employees.SysAdmin, level: Levels.Expert, time: 115 },
    SwarmManagement: { name: "Swarm Management", employee: Employees.SysAdmin, level: Levels.Expert, time: 140 },
};

const Requirements = [
    // Developer components are all raw

    // Designer
    { piece: Pieces.UIElement, requires: Pieces.BlueprintComponent, count: 1 },
    { piece: Pieces.UIElement, requires: Pieces.GraphicsComponent, count: 1 },

    { piece: Pieces.UISet, requires: Pieces.WireframeComponent, count: 2 },
    { piece: Pieces.UISet, requires: Pieces.UIElement, count: 2 },

    { piece: Pieces.ResponsiveUI, requires: Pieces.WireframeComponent, count: 2 },
    { piece: Pieces.ResponsiveUI, requires: Pieces.UIElement, count: 2 },

    { piece: Pieces.DesignGuidelines, requires: Pieces.DocumentationComponent, count: 3 },
    { piece: Pieces.DesignGuidelines, requires: Pieces.ResponsiveUI, count: 3 },

    // Lead Developer
    { piece: Pieces.InterfaceModule, requires: Pieces.UIElement, count: 2 },
    { piece: Pieces.InterfaceModule, requires: Pieces.WireframeComponent, count: 1 },

    { piece: Pieces.FrontendModule, requires: Pieces.UIElement, count: 1 },
    { piece: Pieces.FrontendModule, requires: Pieces.InterfaceModule, count: 1 },

    { piece: Pieces.BackendModule, requires: Pieces.BackendComponent, count: 1 },
    { piece: Pieces.BackendModule, requires: Pieces.NetworkComponent, count: 1 },

    { piece: Pieces.InputModule, requires: Pieces.UIComponent, count: 1 },
    { piece: Pieces.InputModule, requires: Pieces.BackendComponent, count: 1 },

    { piece: Pieces.StorageModule, requires: Pieces.FilesystemComponent, count: 1 },
    { piece: Pieces.StorageModule, requires: Pieces.BackendComponent, count: 1 },

    { piece: Pieces.ContentManagementModule, requires: Pieces.FrontendModule, count: 1 },
    { piece: Pieces.ContentManagementModule, requires: Pieces.InputModule, count: 1 },
    { piece: Pieces.ContentManagementModule, requires: Pieces.BackendModule, count: 1 },

    { piece: Pieces.VideoPlaybackModule, requires: Pieces.VideoComponent, count: 1 },
    { piece: Pieces.VideoPlaybackModule, requires: Pieces.FrontendModule, count: 1 },
    { piece: Pieces.VideoPlaybackModule, requires: Pieces.BackendModule, count: 1 },

    { piece: Pieces.SeoModule, requires: Pieces.FrontendModule, count: 1 },
    { piece: Pieces.SeoModule, requires: Pieces.SemanticComponent, count: 1 },

    { piece: Pieces.AuthenticationModule, requires: Pieces.DatabaseComponent, count: 1 },
    { piece: Pieces.AuthenticationModule, requires: Pieces.EncryptionComponent, count: 1 },
    { piece: Pieces.AuthenticationModule, requires: Pieces.BackendModule, count: 1 },

    { piece: Pieces.EmailModule, requires: Pieces.SmtpComponent, count: 1 },
    { piece: Pieces.EmailModule, requires: Pieces.BackendComponent, count: 1 },

    { piece: Pieces.DatabaseLayer, requires: Pieces.DatabaseComponent, count: 1 },
    { piece: Pieces.DatabaseLayer, requires: Pieces.BackendComponent, count: 1 },
    { piece: Pieces.DatabaseLayer, requires: Pieces.NetworkComponent, count: 1 },

    { piece: Pieces.NotificationModule, requires: Pieces.UIComponent, count: 1 },
    { piece: Pieces.NotificationModule, requires: Pieces.BackendComponent, count: 1 },
    { piece: Pieces.NotificationModule, requires: Pieces.EmailModule, count: 1 },

    { piece: Pieces.PaymentGatewayModule, requires: Pieces.DatabaseLayer, count: 1 },
    { piece: Pieces.PaymentGatewayModule, requires: Pieces.ApiClientModule, count: 1 },
    { piece: Pieces.PaymentGatewayModule, requires: Pieces.AuthenticationModule, count: 1 },

    { piece: Pieces.LocalizationModule, requires: Pieces.i18nComponent, count: 1 },
    { piece: Pieces.LocalizationModule, requires: Pieces.FrontendModule, count: 1 },

    { piece: Pieces.SearchModule, requires: Pieces.DatabaseComponent, count: 1 },
    { piece: Pieces.SearchModule, requires: Pieces.SearchAlgorithmComponent, count: 1 },
    { piece: Pieces.SearchModule, requires: Pieces.BackendComponent, count: 1 },

    { piece: Pieces.BandwidthCompressionModule, requires: Pieces.CompressionComponent, count: 1 },
    { piece: Pieces.BandwidthCompressionModule, requires: Pieces.NetworkComponent, count: 1 },
    { piece: Pieces.BandwidthCompressionModule, requires: Pieces.BackendComponent, count: 1 },

    { piece: Pieces.ApiClientModule, requires: Pieces.BackendModule, count: 1 },
    { piece: Pieces.ApiClientModule, requires: Pieces.DatabaseLayer, count: 1 },
    { piece: Pieces.ApiClientModule, requires: Pieces.CompressionComponent, count: 2 },

    { piece: Pieces.CodeOptimizationModule, requires: Pieces.BackendModule, count: 2 },
    { piece: Pieces.CodeOptimizationModule, requires: Pieces.DatabaseLayer, count: 2 },
    { piece: Pieces.CodeOptimizationModule, requires: Pieces.FrontendModule, count: 2 },

    // SysAdmin
    { piece: Pieces.VirtualContainer, requires: Pieces.VirtualHardware, count: 1 },
    { piece: Pieces.VirtualContainer, requires: Pieces.OperatingSystem, count: 1 },
    { piece: Pieces.VirtualContainer, requires: Pieces.ProcessManagement, count: 1 },
    { piece: Pieces.VirtualContainer, requires: Pieces.ContinuousIntegration, count: 1 },
    { piece: Pieces.VirtualContainer, requires: Pieces.CronJob, count: 1 },

    { piece: Pieces.Cluster, requires: Pieces.VirtualContainer, count: 3 },
    { piece: Pieces.Cluster, requires: Pieces.Firewall, count: 10 },

    { piece: Pieces.SwarmManagement, requires: Pieces.Cluster, count: 1 },
    { piece: Pieces.SwarmManagement, requires: Pieces.VirtualContainer, count: 1 },
];

const CompilePlan = (list) => {
    let pieces = [list];
    list.forEach((count, piece) => {
        pieces.push(GetRequirements(piece, count));
    });
    return MergeMaps(pieces);
};

const GetRequirements = (piece, count) => {
    let pieces = new Map();
    Requirements.forEach(req => {
        if (req.piece === piece) {
            if (pieces.has(piece)) {
                pieces.set(req.requires, (count * req.count) + pieces.get(piece));
            } else {
                pieces.set(req.requires, count * req.count);
            }
        }
    });
    let reqPieces = [pieces];
    pieces.forEach((reqCount, reqPiece) => {
        reqPieces.push(GetRequirements(reqPiece, reqCount));
    });
    return MergeMaps(reqPieces);
};

const MergeMaps = (maps) => {
    let merged = maps[0];
    for (let i = 1; i < maps.length; i++) {
        maps[i].forEach((count, piece) => {
            if (merged.has(piece)) {
                merged.set(piece, count + merged.get(piece));
            } else {
                merged.set(piece, count);
            }
        });
    }
    return merged;
};

export default {
    Employees,
    Levels,
    Pieces,
    Requirements,
    CompilePlan,
    GetRequirements,
    MergeMaps
}